﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Text.Json.Serialization;

namespace IdentityServerEntityFramework.Quickstart.MultiTenant
{
    [Table("tbl_User")]
    public class Employee
    { 

        [Column("EMP_ID")]
        public int Id { get; set; }

        [Required, MaxLength(20)]
        [Column("emp_prefferredName")]
        public string Firstname { get; set; }

        [Required, MaxLength(30)]
        [Column("emp_lastName")]
        public string Lastname { get; set; }
        
        [Column("emp_emailOffice")]
        [Required, MaxLength(254)]
        public string Email { get; set; }

        [Required, MaxLength(254)]
        [Column("emp_userName")]
        public string Username { get; set; }


        [Required, MaxLength(255)]
        [Column("emp_passWord")]
       //[JsonIgnore]
        public string Password { get; set; }
        
        [Column("emp_active")]
        [Required]
        public bool Active { get; set; }
        
        [NotMapped]
        public string SubjectId { get; set; }

        [NotMapped]
        public string CustomerId { get; set; }
    }
}