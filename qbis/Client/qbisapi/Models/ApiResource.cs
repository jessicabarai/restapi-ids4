﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace qbisapi.Models
{
    public partial class ApiResource
    {
        public ApiResource()
        {
            ApiResourceClaims = new HashSet<ApiResourceClaim>();
            ApiResourceProperties = new HashSet<ApiResourceProperty>();
            ApiResourceScopes = new HashSet<ApiResourceScope>();
            ApiResourceSecrets = new HashSet<ApiResourceSecret>();
        }

        public int Id { get; set; }
        public bool Enabled { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public string AllowedAccessTokenSigningAlgorithms { get; set; }
        public bool ShowInDiscoveryDocument { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? LastAccessed { get; set; }
        public bool NonEditable { get; set; }

        public virtual ICollection<ApiResourceClaim> ApiResourceClaims { get; set; }
        public virtual ICollection<ApiResourceProperty> ApiResourceProperties { get; set; }
        public virtual ICollection<ApiResourceScope> ApiResourceScopes { get; set; }
        public virtual ICollection<ApiResourceSecret> ApiResourceSecrets { get; set; }
    }
}
