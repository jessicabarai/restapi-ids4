﻿using Microsoft.EntityFrameworkCore;
using IdentityServerEntityFramework.Quickstart.MultiTenant;

namespace IdentityServerEntityFramework.Quickstart.MultiTenant
{
    public class dbQBISSYSSQLDbContext : DbContext
    {
        public dbQBISSYSSQLDbContext(DbContextOptions options) : base (options) { }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<IdentityServerEntityFramework.Quickstart.MultiTenant.Employee> Employee { get; set; }
    }
}
