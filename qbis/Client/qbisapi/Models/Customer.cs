﻿using System.ComponentModel.DataAnnotations.Schema;

namespace IdentityServerEntityFramework.Quickstart.MultiTenant
{
    [Table("tbl_Customers")]
    public class Customer
    {
        [Column("CUS_ID")]
        public int Id { get; set; }

        [Column("cus_name")]
        public string Name { get; set; }

        [Column("cus_dbKey")]
        public string DBName { get; set; }

        [Column("cus_active")]
        public char Active { get; set; }

        [Column("cus_connectionstring")]
        public string ConnectionString { get; set; }
    }
}
