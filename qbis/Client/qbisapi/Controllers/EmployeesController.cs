﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IdentityServerEntityFramework.Quickstart.MultiTenant;
using Microsoft.AspNetCore.Authorization;

namespace qbisapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EmployeesController : ControllerBase
    {
        private readonly dbQBISSYSSQLDbContext _context;

       
        public EmployeesController(dbQBISSYSSQLDbContext context)
        {
            _context = context;
        }

        // GET: api/Employees
        /// <summary>
        /// Return all employee for the user's company
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Employee>>> GetEmployee()
        {

            var r = this.Request.HttpContext.User.Claims.Where(x => x.Type == "customer_id").Single().Value;
            //var v = await UserRepository.GetCompanyAndUserName(r);
            //id instead of name
            var c = await _context.Customers.Where(x => x.Id == Int32.Parse(r)).FirstOrDefaultAsync();

            var d = c.ConnectionString;
            CustomersDbContext customerContext = new CustomersDbContext(
                new DbContextOptionsBuilder<CustomersDbContext>().UseSqlServer(d).Options);
            return await customerContext.Employees.ToListAsync();
           // return await _context.Employee.ToListAsync();
        }
        /// <summary>
        /// Returns the employee with the specific ID 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Employees/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Employee>> GetEmployee(int id)
        {

            /* var r = this.Request.HttpContext.User.Claims.Where(x => x.Type == "sub").Single().Value;
             var v = await UserRepository.GetCompanyAndUserName(r);
             var c = await _context.Customers.Where(x => x.Name == v[0]).FirstOrDefaultAsync();
             var d = c.ConnectionString;*/
            var r = this.Request.HttpContext.User.Claims.Where(x => x.Type == "customer_id").Single().Value;
            //var v = await UserRepository.GetCompanyAndUserName(r);
            //id instead of name
            var c = await _context.Customers.Where(x => x.Id == Int32.Parse(r)).FirstOrDefaultAsync();

            var d = c.ConnectionString;
            CustomersDbContext customerContext = new CustomersDbContext(
                new DbContextOptionsBuilder<CustomersDbContext>().UseSqlServer(d).Options);
           // return await customerContext.Employees.ToListAsync();
            var employee = await customerContext.Employees.FindAsync(id);

            if (employee == null)
            {
                return NotFound();
            }

            return employee;
        }

       
        // PUT: api/Employees/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmployee(int id, Employee employee)
        {
            if (id != employee.Id)
            {
                return BadRequest();
            }

            _context.Entry(employee).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        /// <remarks>
        /// Sample request:
        ///     POST{
        ///    
        ///
        ///       "firstname": "David",
        ///       "lastname": "Fagergren",
        ///       "email": "mejlbarai@gmail.com",
        ///       "username": "test",
        ///       "password": "Jessicaärbäst",
        ///       "active": true,
        ///       "subjectId": null,
        ///       "customerId": null
        ///   
        /// 
        /// 
        /// }
        /// </remarks>
        /// <response code="201">Returns the newly created item</response>
        /// <response code="400">If the item is null</response>   
        // POST: api/Employees
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Employee>> PostEmployee(Employee employee)
        {

            var r = this.Request.HttpContext.User.Claims.Where(x => x.Type == "customer_id").Single().Value;
            //var v = await UserRepository.GetCompanyAndUserName(r);
            //id instead of name
            var c = await _context.Customers.Where(x => x.Id == Int32.Parse(r)).FirstOrDefaultAsync();

            var d = c.ConnectionString;
            /* var r = this.Request.HttpContext.User.Claims.Where(x => x.Type == "sub").Single().Value;
             var v = await UserRepository.GetCompanyAndUserName(r);
             //change to x.id, i'm a  bit confused here ask Can.
             var c = await _context.Customers.Where(x => x.Name == v[0]).FirstOrDefaultAsync();
             var d = c.ConnectionString;*/
            CustomersDbContext customerContext = new CustomersDbContext(
                new DbContextOptionsBuilder<CustomersDbContext>().UseSqlServer(d).Options);
            //check dumplicates for username
            //do I need to return bad request or entity framework does  it for me for violating char limits.
           if (customerContext.Employees.Any(x => x.Username == employee.Username)) { 
                return BadRequest("Username already exists");
            }
            //hash password
            employee.Password = Security.HashPassword(employee.Password);
            customerContext.Employees.Add(employee);
            await customerContext.SaveChangesAsync();
            //return BadRequest();
            return CreatedAtAction("GetEmployee", new { id = employee.Id }, employee);
        }

        // DELETE: api/Employees/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Employee>> DeleteEmployee(int id)
        {
            var r = this.Request.HttpContext.User.Claims.Where(x => x.Type == "customer_id").Single().Value;
            //var v = await UserRepository.GetCompanyAndUserName(r);
            //id instead of name
            var c = await _context.Customers.Where(x => x.Id == Int32.Parse(r)).FirstOrDefaultAsync();

            var d = c.ConnectionString;
            /*var r = this.Request.HttpContext.User.Claims.Where(x => x.Type == "sub").Single().Value;
            var v = await UserRepository.GetCompanyAndUserName(r);
            //change to x.id 
            var c = await _context.Customers.Where(x => x.Name == v[0]).FirstOrDefaultAsync();
            var d = c.ConnectionString;*/
            CustomersDbContext customerContext = new CustomersDbContext(
                new DbContextOptionsBuilder<CustomersDbContext>().UseSqlServer(d).Options);

            var employee = await customerContext.Employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound();
            }

            customerContext.Employees.Remove(employee);
            await customerContext.SaveChangesAsync();

            return employee;
        }

        private bool EmployeeExists(int id)
        {
            return _context.Employee.Any(e => e.Id == id);
        }
    }
}
