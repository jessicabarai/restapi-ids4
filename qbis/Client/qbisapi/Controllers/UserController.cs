﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using qbisapi.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace qbisapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        [HttpGet]
         public IEnumerable<AspNetUser> Get()
         {
             using (var context = new aspnet53bc9b9d9d6a45d484292a2761773502Context())
             {
                 return (IEnumerable<AspNetUser>)context.AspNetUsers.ToList();

             }


         }

     }
    }

