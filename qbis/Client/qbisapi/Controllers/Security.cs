﻿using System.Text;
using Microsoft.EntityFrameworkCore;
using System;
using System.Security.Cryptography;
using System.Threading.Tasks;
namespace qbisapi.Controllers
{
    public class Security
    {

        public static bool CheckHashedPassword(string receivedHashedPpassword, string hashedDBPassword)
        {
            return (receivedHashedPpassword.ToLower() == hashedDBPassword.ToLower());
        }

        public static string HashPassword(string password)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(password));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    sb.Append(b.ToString("x2"));   // Lower case
                }

                return sb.ToString();
            }
        }
    }
}
    

