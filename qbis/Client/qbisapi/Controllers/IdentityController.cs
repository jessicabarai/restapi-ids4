﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;

namespace qbisapi.Controllers

{
    [Route("qbisdata")]
    //[Authorize]
    public class IdentityController : ControllerBase
    {
        SqlConnection con = new SqlConnection("Server=(localdb)\\mssqllocaldb;Database=aspnet-53bc9b9d-9d6a-45d4-8429-2a2761773502;Trusted_Connection=True;MultipleActiveResultSets=true");
        [HttpGet]
        public IEnumerable<string> Get()
        {
            SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM Test", con);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                yield return JsonConvert.SerializeObject(dt);
                // return new JsonResult(dt);
            }
            yield return new string("No data");

            //return new JsonResult(from c in User.Claims select new { c.Type, c.Value });
        }
        //TODO 31/3 create a valid APIweb
        [HttpPost]
        public string Post([FromBody]string Name)
        {
            SqlCommand cmd = new SqlCommand("Insert into Test VALUES('" + Name + "')", con);//,/* '"+ClientId+"')", con);
            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();
            if (i == 1)
            {
               return new string("Success inserting data");
            }
            return new string("Failure inserting data");

        }



    }

    
}
