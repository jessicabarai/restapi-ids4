﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using qbisapi.Models;

namespace qbisapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ApiResourcesController : ControllerBase
    {
        private readonly aspnet53bc9b9d9d6a45d484292a2761773502Context _context;

        public ApiResourcesController(aspnet53bc9b9d9d6a45d484292a2761773502Context context)
        {
            _context = context;
        }

        // GET: api/ApiResources
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ApiResource>>> GetApiResources()
        {
            return await _context.ApiResources.ToListAsync();
        }

        // GET: api/ApiResources/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ApiResource>> GetApiResource(int id)
        {
            var apiResource = await _context.ApiResources.FindAsync(id);

            if (apiResource == null)
            {
                return NotFound();
            }

            return apiResource;
        }

        // PUT: api/ApiResources/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutApiResource(int id, ApiResource apiResource)
        {
            if (id != apiResource.Id)
            {
                return BadRequest();
            }

            _context.Entry(apiResource).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApiResourceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ApiResources
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ApiResource>> PostApiResource(ApiResource apiResource)
        {
            _context.ApiResources.Add(apiResource);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetApiResource", new { id = apiResource.Id }, apiResource);
        }

        // DELETE: api/ApiResources/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ApiResource>> DeleteApiResource(int id)
        {
            var apiResource = await _context.ApiResources.FindAsync(id);
            if (apiResource == null)
            {
                return NotFound();
            }

            _context.ApiResources.Remove(apiResource);
            await _context.SaveChangesAsync();

            return apiResource;
        }

        private bool ApiResourceExists(int id)
        {
            return _context.ApiResources.Any(e => e.Id == id);
        }
    }
}
