using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.OpenApi.Models;
using Microsoft.EntityFrameworkCore;
using qbisapi.Models;
using IdentityServerEntityFramework.Quickstart.MultiTenant;
using System.Reflection;
using System.IO;

namespace qbisapi
{
    public class Startup
    {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        
        }
        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<dbQBISSYSSQLDbContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("dbQBISSYSSQL")));
           // services.AddDbContext<dbQBISSYSSQLDbContext>(options =>
           // options.UseSqlServer(Configuration.GetConnectionString("dbQBISSYSSQL")));
            // services.AddDbContext<aspnet53bc9b9d9d6a45d484292a2761773502Context>(options =>
            //  options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            ConfigureSwagger(services);
            services.AddSwaggerGen(c =>
            {
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication("Bearer", options =>
                {
                    options.Authority = "https://localhost:5001";
                    options.ApiName = "qbis";
                    /* options.TokenValidationParameters = new TokenValidationParameters
                     {
                         ValidateAudience = false
                     };*/
                });
        }

        private static void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "qbis", Version = "v1" });
            });
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            { 
            c.SwaggerEndpoint("/swagger/v1/swagger.json", "qbis API V1");
             });
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
