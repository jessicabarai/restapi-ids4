﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;

namespace IdentityServerEntityFramework.Quickstart.MultiTenant
{
    [Table("tbl_User")]
    public class Employee
    {
        [Column("EMP_ID")]
        public int Id { get; set; }

        [Column("emp_prefferredName")]
        public string Firstname { get; set; }
        
        [Column("emp_lastName")]
        public string Lastname { get; set; }
        
        [Column("emp_emailOffice")]
        public string Email { get; set; }

        [Column("emp_userName")]
        public string Username { get; set; }

        [Column("emp_passWord")]
        public string Password { get; set; }
        
        [Column("emp_active")]
        public bool Active { get; set; }

        [NotMapped]
        public int CompanyId{ get; set; }

        [NotMapped]
        public string SubjectId { get; set; }
    }
}