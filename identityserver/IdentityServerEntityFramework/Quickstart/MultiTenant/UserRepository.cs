﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using Microsoft.EntityFrameworkCore;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace IdentityServerEntityFramework.Quickstart.MultiTenant
{
    public class UserRepository
    {
        private Customer _customer;
        private CustomersDbContext _dbContext;

        public UserRepository(Customer customer) 
        {
            _customer = customer;
            _dbContext = new CustomersDbContext(
                new DbContextOptionsBuilder<CustomersDbContext>().UseSqlServer(customer.ConnectionString).Options);
        }

        public async Task<Employee> GetUserByUsername(string username)
        {
            try
            {
                Employee employee =  await _dbContext.Employees
                    .SingleOrDefaultAsync(employee => employee.Username == username);

                if (employee != null) 
                {
                    employee.SubjectId = await GetSubjectId(_customer.Name, employee.Username);
                    employee.CompanyId = _customer.Id;
                }

                return employee;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<bool> CheckPassword(Employee employee, string password) 
        {
            return await Task.FromResult(CheckHashedPassword(HashPassword(password), employee.Password));
        }


        // TODO: Move following methods in Common.cs class, they don't need to be here
      
        public static async Task<string> GetSubjectId(string customerName, string username) 
        {
            var subject = $"{customerName}:{username}";
            var subjectBytes = Encoding.UTF8.GetBytes(subject);

            return await Task.FromResult(Convert.ToBase64String(subjectBytes));
        }

        public static async Task<string[]> GetCompanyAndUserName(string subjectId)
        {
            byte[] data = Convert.FromBase64String(subjectId);
            string companyUsername = Encoding.UTF8.GetString(data);


            return await Task.FromResult(companyUsername.Split(':'));
        }

        // TODO: Following codes should be moved to a new Security class
        private bool CheckHashedPassword(string receivedHashedPpassword, string hashedDBPassword)
        {
            return (receivedHashedPpassword.ToLower() == hashedDBPassword.ToLower());
        }

        private string HashPassword(string password) 
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(password));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    sb.Append(b.ToString("x2"));   // Lower case
                }

                return sb.ToString();
            }
        }
    }
}