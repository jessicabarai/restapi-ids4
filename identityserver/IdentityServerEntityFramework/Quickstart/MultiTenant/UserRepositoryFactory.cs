﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace IdentityServerEntityFramework.Quickstart.MultiTenant
{
    internal class UserRepositoryFactory : IUserRepositoryFactory
    {
        private dbQBISSYSSQLDbContext _dbContext;

        public UserRepositoryFactory (string connectionString)
        {
            _dbContext = new dbQBISSYSSQLDbContext(
                new DbContextOptionsBuilder<dbQBISSYSSQLDbContext>().UseSqlServer(connectionString).Options);
        }

        public async Task<UserRepository> GetUserRepositoryForCompany(string name)
        {
            var company = await GetCompanyByName(name);
            
            if (company != null && company.Active == '1') 
            {
                return new UserRepository(company);
            }

            return null;
        }

        private async Task<Customer> GetCompanyByName(string name) 
        { 
            try 
            {
                return await _dbContext.Customers.SingleOrDefaultAsync(customer => customer.Name == name);
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}