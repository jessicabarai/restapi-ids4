﻿using Microsoft.EntityFrameworkCore;
using IdentityServerEntityFramework.Quickstart.MultiTenant;

namespace IdentityServerEntityFramework.Quickstart.MultiTenant
{
    public class CustomersDbContext : DbContext 
    {
        public CustomersDbContext(DbContextOptions options) : base(options) { }

        public DbSet<Employee> Employees { get; set; }
    }
}
