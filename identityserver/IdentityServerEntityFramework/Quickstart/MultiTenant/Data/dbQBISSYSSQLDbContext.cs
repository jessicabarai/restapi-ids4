﻿using Microsoft.EntityFrameworkCore;

namespace IdentityServerEntityFramework.Quickstart.MultiTenant
{
    public class dbQBISSYSSQLDbContext : DbContext
    {
        public dbQBISSYSSQLDbContext(DbContextOptions options) : base (options) { }

        public DbSet<Customer> Customers { get; set; }
    }
}
