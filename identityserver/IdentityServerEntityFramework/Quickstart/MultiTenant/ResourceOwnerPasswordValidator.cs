﻿using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentityServerEntityFramework.Quickstart.MultiTenant
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        //repository to get user from db
        private readonly IUserRepositoryFactory _userRepositoryFactory;

        public ResourceOwnerPasswordValidator(IUserRepositoryFactory userRepositoryFactory)
        {
            _userRepositoryFactory = userRepositoryFactory; 
        }

        //this is used to validate your user account with provided grant at /connect/token
        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            try
            {   
                var username = context.UserName;
                var password = context.Password;
                var company = context.Request.Raw.Get("Company");

                if (string.IsNullOrWhiteSpace(company))
                {
                    context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Company is required.");
                    return;
                }

                if (string.IsNullOrWhiteSpace(username))
                {
                    context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Username is required.");
                    return;
                }

                var userRepository = await _userRepositoryFactory.GetUserRepositoryForCompany(company);
                if (userRepository == null)
                {
                    context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, $"No company found for {company}.");
                    return;
                }

                var user = await userRepository.GetUserByUsername(username);
                if (user == null)
                {
                    context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, $"No user {username} found for {company}.");
                    return;
                }

                // Validate user password
                if (await userRepository.CheckPassword(user, password)) 
                {
                    context.Result = new GrantValidationResult(user.SubjectId, GrantType.ResourceOwnerPassword, GetUserClaims(user));
                }
                else
                {
                    context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Invalid username or password.");
                }
            }
            catch (Exception ex)
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Invalid username or password");
            }
        }

        //build claims array from user data
        public static Claim[] GetUserClaims(Employee employee)
        {   // get the customer id from this employee
            // Add as a claim and then get the connection string from the id instead of the name
            return new Claim[]
            //should add a subject id claim
            
            {   new Claim(JwtClaimTypes.Subject, employee.SubjectId  ?? ""),
                new Claim("employee_id", employee.Id.ToString() ?? ""),
                new Claim("customer_id", employee.CompanyId.ToString() ?? ""),
               //  new Claim("Customer_id", employee.ToString() ?? ""),
              //  new Claim(JwtClaimTypes.Name, (!string.IsNullOrEmpty(employee.Firstname) && !string.IsNullOrEmpty(employee.Lastname)) ? (employee.Firstname + " " + employee.Lastname) : ""),
               // new Claim(JwtClaimTypes.GivenName, employee.Firstname  ?? ""),
               // new Claim(JwtClaimTypes.FamilyName, employee.Lastname  ?? ""),
               // new Claim(JwtClaimTypes.Email, employee.Email  ?? ""),

                //roles
                //new Claim(JwtClaimTypes.Role, employee.Role)
                
            };
        }
    }
}
