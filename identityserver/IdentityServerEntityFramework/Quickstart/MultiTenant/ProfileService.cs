﻿using IdentityServer4.Models;
using IdentityServer4.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentityServerEntityFramework.Quickstart.MultiTenant
{
    public class ProfileService : IProfileService
    {
        //services
        private readonly IUserRepositoryFactory _userRepositoryFactory;
        
        public ProfileService(IUserRepositoryFactory userRepositoryFactory)
        {
            _userRepositoryFactory = userRepositoryFactory;
            
            
        }

        //Get user profile data in terms of claims when calling /connect/userinfo
        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            try
            {
                //depending on the scope accessing the user data.
                if (!string.IsNullOrEmpty(context.Subject.Identity.Name))
                {
                    var decodedSubject = await UserRepository.GetCompanyAndUserName(context.Subject.Identity.Name);
                    var company = decodedSubject[0];
                    var username = decodedSubject[1];
                    
                    var userRepository = await _userRepositoryFactory.GetUserRepositoryForCompany(company);

                    if (userRepository != null)
                    {
                        var user = await userRepository.GetUserByUsername(username);

                        if (user != null)
                        {
                            var claims = ResourceOwnerPasswordValidator.GetUserClaims(user);

                            //set issued claims to return, // need to filter this 
                            context.IssuedClaims = claims.Where(x => context.RequestedClaimTypes.Contains(x.Type)).ToList();
                        }
                    }
                }
                else
                {
                    //get subject from context (this was set ResourceOwnerPasswordValidator.ValidateAsync),
                    //where and subject was set to my user id.
                    var subjectId = context.Subject.Claims.FirstOrDefault(x => x.Type == "sub");

                    if (!string.IsNullOrEmpty(subjectId?.Value))
                    {
                        //get user from db (find user by user id)
                        //var user = await _userRepository.FindAsync();

                        var decodedSubject = await UserRepository.GetCompanyAndUserName(subjectId?.Value);
                        var company = decodedSubject[0];
                        var username = decodedSubject[1];

                        var userRepository = await _userRepositoryFactory.GetUserRepositoryForCompany(company);

                        if (userRepository != null)
                        {
                            var user = await userRepository.GetUserByUsername(username);

                            // issue the claims for the user
                            if (user != null)
                            {
                                var claims = ResourceOwnerPasswordValidator.GetUserClaims(user);
                                context.IssuedClaims.AddRange(claims);
                                //context.IssuedClaims = claims.Where(x => context.RequestedClaimTypes.Contains(x.Type)).ToList();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //log your error
            }
        }

        //check if user account is active.
        public async Task IsActiveAsync(IsActiveContext context)
        {
            try
            {
                //get subject from context (set in ResourceOwnerPasswordValidator.ValidateAsync),
                var userId = context.Subject.Claims.FirstOrDefault(x => x.Type == "user_id");

                if (!string.IsNullOrEmpty(userId?.Value) && long.Parse(userId.Value) > 0)
                {
                    //var user = await _userRepository.FindAsync(long.Parse(userId.Value));

                    var username = "";
                    var company = "";
                    var userRepository = await _userRepositoryFactory.GetUserRepositoryForCompany(company);
                    
                    if (userRepository != null)
                    {
                        var user = await userRepository.GetUserByUsername(username);

                        if (user != null)
                        {
                            if (user.Active)
                            {
                                context.IsActive = user.Active;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //handle error logging
            }
        }
    }
}
