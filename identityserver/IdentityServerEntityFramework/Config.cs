﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityModel;
using IdentityServer4.Models;
using System;
using System.Collections.Generic;

namespace IdentityServerEntityFramework
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
                   new IdentityResource[]
                   {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email(),
                new IdentityResources.Phone(),
                new IdentityResource{
                     Name = "role",
                     UserClaims = new []{ "sub" } }
                   };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope("scope1"),
                new ApiScope("scope2"),
                new ApiScope("qbisapi.read", "qbisapi.write"),
                };

        public static IEnumerable<ApiResource> ApiResources => new[]

        {
            new ApiResource("qbis")
            {
                Scopes = new List<string> {"qbisapi.read", "qbisapi.write"},
                ApiSecrets = new List<Secret> {new Secret("511536EF-F270-4058-80CA-1C89C192F69O".Sha256())},
                UserClaims = {"role", "sub"}}
        };

        public static IEnumerable<Client> Clients =>
            new Client[]
            {

                    new Client
                {
                    ClientId = "implicitClient",
                    ClientName = "implicitClient",

                    AllowedGrantTypes = GrantTypes.Implicit,

                    RedirectUris = { "https://localhost:44300/signin-oidc" },
                    FrontChannelLogoutUri = "https://localhost:44300/signout-oidc",
                    PostLogoutRedirectUris = { "https://localhost:44300/signout-callback-oidc" },

                    ClientSecrets = { new Secret("511536EF-F270-4058-80CA-1C89C192F69C".Sha256())},
                    AllowOfflineAccess = true,
                    AllowAccessTokensViaBrowser = true,
                    AllowedScopes = { "scope1", "openid", "profile", "email", "phone" },
                    RequirePkce = true
                },
                // m2m client credentials flow client
                new Client
                {
                    ClientId = "m2m.client",
                    ClientName = "Client Credentials Client",

                    AllowedGrantTypes =  GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                    ClientSecrets = { new Secret("511536EF-F270-4058-80CA-1C89C192F69A".Sha256())},
                    AllowOfflineAccess = true,
                    AllowedScopes = { "qbisapi.read", "qbisapi.write", "role"},
                    RequirePkce = true
                },

                // interactive client using code flow + pkce
                new Client
                {
                    ClientId = "interactive",
                    ClientSecrets = { new Secret("49C1A7E1-0C79-4A89-A3D6-A37998FB86B0".Sha256()) },

                    AllowedGrantTypes = GrantTypes.Code,

                    RedirectUris = { "https://localhost:44300/signin-oidc" },
                    FrontChannelLogoutUri = "https://localhost:44300/signout-oidc",
                    PostLogoutRedirectUris = { "https://localhost:44300/signout-callback-oidc" },

                    AllowOfflineAccess = true,
                    AllowedScopes = { "openid", "profile", "scope2", "email", "phone" },

                    RequirePkce = false
                },
            };
    }
}
